package ejercicio1;

public class Articulo {
	
	private String nombreArticulo;
	private int precio;
	
	public Articulo () {
		
	}
	
	public Articulo (String nombArt, int p) {
		nombreArticulo = nombArt;
		precio = p;
	}

	public String getNombreArticulo() {
		return nombreArticulo;
	}

	public void setNombreArticulo(String nombreArticulo) {
		this.nombreArticulo = nombreArticulo;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

}
