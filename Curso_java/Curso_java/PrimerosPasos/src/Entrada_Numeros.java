import javax.swing.JOptionPane;

public class Entrada_Numeros {

	public static void main(String[] args) {
		
		String num1 = JOptionPane.showInputDialog("Introduce un n�mero");
		double n1 = Double.parseDouble(num1);
		
		System.out.print("La raiz de " + n1 + " es ");
		System.out.printf("%1.3f", Math.sqrt(n1));

	}

}
