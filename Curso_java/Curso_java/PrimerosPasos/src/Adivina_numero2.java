
import java.util.*;

public class Adivina_numero2 {

	public static void main(String[] args) {
		
		int aleatorio = (int)(Math.random()*100);
		
		Scanner entrada = new Scanner(System.in);
		
		int numero = 0;
		int intentos = 0;
		
		do{
			
			System.out.print("introduce un numero, por favor: ");
			numero = entrada.nextInt();
			
			if(aleatorio < numero ) {
				
				System.out.println("Intenta otro m�s bajo");
			} else if (aleatorio > numero) {
				
				System.out.println("Intenta otro m�s alto");
			}
			intentos++;
		}while(numero != aleatorio);
		
		System.out.println("El numero es correcto!!, era el " + aleatorio + " y lo adivinaste en " + intentos + " intentos");
		
	}

}
