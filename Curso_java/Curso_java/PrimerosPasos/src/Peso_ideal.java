
import javax.swing.*;
import java.util.*;

public class Peso_ideal {

	public static void main(String[] args) {

		String genero = "";
		int altura;
		int pesoIdeal = 0;

		do {

			genero = JOptionPane.showInputDialog("Introduce tu genero (H/M)");
			
		} while (genero.equalsIgnoreCase("H") == false && genero.equalsIgnoreCase("M") == false);
		
		altura= Integer.parseInt(JOptionPane.showInputDialog("Introduce tu altura en cm (000)"));
		
		if(genero.equalsIgnoreCase("H")) {
			pesoIdeal = altura-110;
		} else if (genero.equalsIgnoreCase("M")) {
			pesoIdeal=altura-120;
		}
		
		System.out.println("Tu peso ideal es "+ pesoIdeal + "Kg");
	}

}
