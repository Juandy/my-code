import java.util.*;

public class Entrada_ejemplo1 {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		String nombreUsuario;
		int edad;

		System.out.print("Introduce tu nombre, por favor :");
		nombreUsuario = entrada.nextLine();
		System.out.print("Introduce tu edad: ");
		edad = entrada.nextInt();

		System.out.println("Hola " + nombreUsuario + ", el a�o que viene tendr�s " + (edad+1) + " a�os.");

	}

}
