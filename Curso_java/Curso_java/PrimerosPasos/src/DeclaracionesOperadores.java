
public class DeclaracionesOperadores {

	public static void main(String[] args) {

		final double aPulgadas = 2.54;

		double cm = 6;

		double resultado = cm / aPulgadas;

		System.out.println("En " + cm + " cm hay " + resultado + " pulgadas");

		int op1, op2, res;
		op1 = 5;
		op2 = 5;
		res = op1 + op2;
		System.out.println(res);
	}

}
