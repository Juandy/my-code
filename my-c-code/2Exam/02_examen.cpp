#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 10

int  main (){

    int potencia = 5;
    double f = 0, resultado = 1;
    int x = 2x

    double c [MAX] = {1,2,3,4,5}; /*Plinomio (termino)*/

    /*Desarrollo en papel del ejercicio
     * 1·x^0 + 2·x^1 + 3·x^2 + 4·x^3 + 5·x^4
     c[0]·x^0 + c[1]·x^1 + c[2]·x^2 + c[3]·x^3 + c[4]·x^4
     c[i]·x^i
     f+=c[i]·x^i*/

    printf ("Limite inferior: ");
    scanf  ("%lf",&x);

    for (int termino=0;termino<MAX; termino++)
        f+=c[termino] * pow (x, termino);

    printf ("f(%i) = %.2lf\n",x,f);

    for (int termino=0;termino<MAX; termino++){
        exponente = 1;
        for (int vez=0; vez<termino; vez++)
            exponente *= x;
        f+=c[termino] * exponente;
    }

    printf ("f(%i) = %.2lf\n",x,f);

    return EXIT_SUCCESS;
}
