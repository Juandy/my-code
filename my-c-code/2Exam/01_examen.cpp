#include <stdio.h>
#include <stdlib.h>


#define M 2
#define K 4
#define N 3

int  main (){

    int i,
        j;

      double A[M][K] = {
                        {2, 3, 5, 1},
                        {3, 1, 4, 2}
                       },
             B[K][N] = {
                         {5, 2, 1},
                         {3,-7, 2},
                         {-4,5, 1},
                         {2, 3,-9}
                        },
             C[M][N];

    printf("Fila:" );
    scanf (" %i",&i);
    printf("Columna:" );
    scanf (" %i",&j);

    C[i][j]=0;
    for(int k=0; k<K; k++)
        C[i][j] += A[i][k] * B[k][j];
    /*c12 vale inicialmente 0
      cuando k vale 0 vas a meter a10 * b02
      cuando k vale 1 vas a meter a11 *b12
      cuando k vale 2 vas a meter a12 *b22
      cuando k vale 3 vas a meter a13 *b32
      cuando k vale 4 (k<K)||(4<4) no pues se sale del bucle
      */

    printf("C[%i][%i]=%.2lf \n", i,j,C[i][j]);

    return EXIT_SUCCESS;
}
