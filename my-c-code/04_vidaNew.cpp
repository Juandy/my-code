//
//  Autor: Juan Diego Fernandez Arroyave
//  Fecha: 25 de enero del 2019
//  Copyright
//
//  Programa que pregunta  tu nombre y fecha de nacimiento.
//  Para Calcular tu edad y los dias vividos hasta la fecha actual
//

//Librerias
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

//define
#define N 100
#define DELAY 10000


//Entrada de Variables
int main (){

    char nombre [100];
    int edad;
    int diaN,
        mesN,
        anioN;
    int diaH,
        mesH,
        anioH;
    int diaV,
        mesV,
        anioV;
    //Entrada de datos
    printf (" Cual es tu nombre. \n");           //Pregunta tu nombre
    fgets  (nombre, 100, stdin);
    printf (" ¿Cuando naciste dd/mm/aaa?\n");    //Pregunta tu dia de nacimiento
    scanf  (" %d/%d/%d",&diaN, &mesN, &anioN);
    printf (" ¿Fecha de hoy dd/mm/aaaa?\n");
    scanf  (" %d/%d/%d",&diaH, &mesH, &anioH);   //Pregunta la fecha actual


    //Calculos                         //Calculos de la edad y dias vividos
    diaV = diaH - diaN;
    mesV = mesH - mesN;
    anioV = anioH - anioN;

    mesV = mesV*30;
    anioV = anioV*365;

    diaV=diaV+mesV+anioV;
    edad=anioH-anioN;


    printf ("Calculando dias.\n");
    for (int veces=0; veces<N; veces++){              //bucle de linea de carga
        for (int columna=0; columna<veces;columna++)
            fprintf(stderr, "=");
        fprintf ( stderr, ">%i%%\r",veces);
        usleep (DELAY);
    }

    //Salida de datos
    system ("clear");              //Limpia termnal y muestra el resultado final
    printf (" Hola %s tu edad es de %i años y llevas vivo %i dias\n", nombre, edad,diaV);

    return EXIT_SUCCESS;
}
