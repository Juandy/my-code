package es.pildoras.pruebaannotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

//Con esta anotacion de le decimos a Spring que es clase va a ser nuestro archivo de configuracion
@Configuration
//Con esta anotacion le decimos el nombre del paquete en donde debe escanear el archivo de configuracion
@ComponentScan("es.pildoras.pruebaannotations")
//Con esta anotacion le indicamos a spring donde esta el archivo de propiedades, es decir su ruta
@PropertySource("classpath:datosEmpresa.propiedades")
public class EmpleadosConfig {
	
	
	//Definir el bean para InformeFinancieroDtoCompras
	
	@Bean
	public CreacionInformeFinanciero informeFinancieroDtoCompras() {        //ID del bean inyectado
		
		return new InformeFinancieroDtoCompras();
	}
	
	
	//Definir el bean para el DirectorFinanciero e inyectar dependencias
	
	@Bean
	public Empleados directorFinanciero() {
		
		return new DirectorFinanciero(informeFinancieroDtoCompras());
	}
}
