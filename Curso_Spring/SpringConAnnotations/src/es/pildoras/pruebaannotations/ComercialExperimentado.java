package es.pildoras.pruebaannotations;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component("ComercialExperimentado")//Gracias a esta anotacion Spring genera automaticamente el bean
//si no le pones un nombre por defecto pone el nombre de la clase con la primera letra en minuscula
//@Scope("prototype")
//Por defecto viene en Singleton, y para cambiarlo haba que especificarlo en el xml,
//Ahora con esta etiqueta podemos hacer dicho cambio
public class ComercialExperimentado implements Empleados {

	@Autowired
	@Qualifier("informeFinancieroTrim4")
	//Le decimos a Spring el Bean ID que debe utilizar, esto es en caso de que tengamos varias clases(Beans)
	private CreacionInformeFinanciero nuevoInforme;
	
	public ComercialExperimentado() {} 
	//Si se define un solo constructor en el bean no es necesario utilizar el Autowrite. 
	//La inyeccion de independencia en la V 4.3 se hacen automaticamente  

	/*@Autowired 
	public ComercialExperimentado(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}
	@Autowired
	public void setNuevoInforme(CreacionInformeFinanciero nuevoInforme) {
		this.nuevoInforme = nuevoInforme;
	}*/

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Vender x3";
	}

	@Override
	public String getInformes() {
		// TODO Auto-generated method stub
		return nuevoInforme.getInformeFinanciero();
	}
	
	//Ejecuci�n de  c�digo despu�s de creaci�n del Bean
	@PostConstruct
	public void ejecutaDespuesCreacion() {
		System.out.println("Ejecutado tras creacion de bean");
	}
	//Ejecuci�n de codigo despu�s de apagado contenedor Spring
	@PreDestroy
	public void ejecutaAntesdestruccion() {
		System.out.println("Ejecutando antes de la destruccion");
	}
}
