package es.pildoras.pruebaannotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Cargar XML
		//ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		//Al tener laconfiguracion en java no haria falta la linea anterior
		
		//Leer el class de configuracion
		
		AnnotationConfigApplicationContext contexto = new AnnotationConfigApplicationContext(EmpleadosConfig.class);
		
		// Pedir un bean al contenedor
		Empleados Antonio = contexto.getBean("ComercialExperimentado", Empleados.class);
		Empleados Lucia = contexto.getBean("ComercialExperimentado", Empleados.class);

		// Comprobamos si apuntan al mismo objeto en memoria
		if (Antonio == Lucia) {
			
			System.out.println("Apuntan al mismo lugar en memoria");
			System.out.println(Antonio + "\n" + Lucia);
		} else {
			
			System.out.println("No apuntan al mismo lugar en memoria");
			System.out.println(Antonio + "\n" + Lucia);
		}
		//Cerrar el contexto
		contexto.close();

	}

}
