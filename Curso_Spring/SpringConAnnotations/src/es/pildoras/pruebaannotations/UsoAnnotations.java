package es.pildoras.pruebaannotations;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Cargar XML
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		//Pedir un bean al contenedor
		Empleados Antonio=contexto.getBean("ComercialExperimentado", Empleados.class);
		//usar el bean
		System.out.println(Antonio.getInformes());
		System.out.println(Antonio.getTareas());

		//cerrar el contexto
		contexto.close();

	}

}
