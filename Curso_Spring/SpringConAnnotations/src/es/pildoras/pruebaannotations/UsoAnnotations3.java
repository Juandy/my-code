package es.pildoras.pruebaannotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoAnnotations3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Leer el class de configuracion
		
		AnnotationConfigApplicationContext contexto = new AnnotationConfigApplicationContext(EmpleadosConfig.class);
		
		// Pedir un bean al contenedor
		
		Empleados empleado=contexto.getBean("directorFinanciero", Empleados.class); 
		DirectorFinanciero dFinanciero=contexto.getBean("directorFinanciero", DirectorFinanciero.class);
		
		//Usar Bean
		
		System.out.println(empleado.getTareas());
		System.out.println(empleado.getInformes());
		System.out.println("Email del director: " + dFinanciero.getEmail());
		System.out.println("Nombre de la empresa: " + dFinanciero.getNombreEmpresa());
		//Cerrar el contexto
		contexto.close();

	}

}
