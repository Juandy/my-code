package es.pildoras.IoC;

public class DirectorEmpleado implements Empleados {
	
	//Creacion de campo tipo CreacionInforme(Interfaz previamente creada)
	private CreacionInformes informeNuevo;
	private String nombreEmpresa;
	private String email;
	
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//Creacion de contructor que inyecta la dependencia	
	public DirectorEmpleado(CreacionInformes informeNuevo) {
		
		this.informeNuevo=informeNuevo;
	}
	
	//M�todo init. Ejecutar tareas antes de que el bean est� disponible
	
	public void metodoInicial() {
		
		System.out.println("Dentro del m�todo init. aqui van las tareas a ejecutar \nantes de que el bean este disponile\n");
		
	}
	//M�todo destroy. Ejecutar tareas despu�s de que el bean haya sido utilizado

public void metodoFinal() {
		
		System.out.println("\nDentro del m�todo destroy. aqui van las tareas a ejecutar \ndespu�ss de utilizar el bean");
		
	}
	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestiono la plantilla de empleados y sus nominas";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe de empleados y nomina: " + informeNuevo.getInforme1();
	}

}
