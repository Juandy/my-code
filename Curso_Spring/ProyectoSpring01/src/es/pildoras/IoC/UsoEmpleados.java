package es.pildoras.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleados {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Objetos mediante un xml");
		System.out.println("***********************");
		//1.Cargar archivo de configuracion de xml
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//2.Pedir El Bean Creado en el xml
		JefeEmpleado Paco = contexto.getBean        ("miJefeEmpleado", JefeEmpleado.class);
		DirectorEmpleado Juan = contexto.getBean    ("miDirectorEmpleado", DirectorEmpleado.class);
		SecretarioEmpleado Maria = contexto.getBean ("miSecretarioEmpleado", SecretarioEmpleado.class);
		SecretarioEmpleado Pablo = contexto.getBean ("miSecretarioEmpleado", SecretarioEmpleado.class);
		
		System.out.println("\n*****Jefe*****");
		System.out.println(Paco.getTareas());
		System.out.println(Paco.getInforme());
		System.out.println("Email jefe: " + Paco.getEmail());
		System.out.println(Paco.getNombreEmpresa());

		System.out.println("\n*****Director*****");
		//3.Utilizar el Bean
		System.out.println(Juan.getTareas());
		//Utilizamos el Bean creado para el informe (inyeccion de dependencia)
		System.out.println(Juan.getInforme());
		System.out.println("Email com�n: " +Juan.getEmail());
		System.out.println(Juan.getNombreEmpresa());
		
		System.out.println("\n*****Secretarios*****");
		//Ejemplo de Inyeccion pero con metodo setter
		System.out.println("Informe del secretario Maria: ");
		System.out.println(Maria.getTareas());
		System.out.println(Maria.getInforme());
		System.out.println("Email com�n: " + Maria.getEmail());
		System.out.println(Maria.getNombreEmpresa());
		
		System.out.println("\nInforme del secretario Pablo: ");
		System.out.println(Pablo.getTareas());
		System.out.println(Pablo.getInforme());
		System.out.println("Email com�n: " + Pablo.getEmail1());
		System.out.println(Pablo.getNombreEmpresa());
		
		//4.Cerrar XML
		contexto.close();
		//Podemos crear varios Bean 
		System.out.println("***********************\n");
		
	}

}
