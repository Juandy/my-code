package es.pildoras.IoC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoSiclovidaBean {

	public static void main(String[] args) {
		// Carga de XML
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("applicationContext3.xml");

		// obtencion de Bean

		Empleados Juan = contexto.getBean("miEmpleado", Empleados.class);
		System.out.println(Juan.getInforme());
		
		//Cerrar XML
		contexto.close();

	}
}
