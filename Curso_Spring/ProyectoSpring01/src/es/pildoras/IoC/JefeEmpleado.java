package es.pildoras.IoC;

public class JefeEmpleado implements Empleados {
	
	

	private CreacionInformes informeNuevo;
	private String nombreEmpresa;
	private String email;
	
	public JefeEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}
		
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTareas() {
		
		return "Gestiono y organizo a los empleados y departamentos";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe de mis empleados: " + informeNuevo.getInforme();
	}

}
