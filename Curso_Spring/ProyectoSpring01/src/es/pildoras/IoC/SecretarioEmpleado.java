package es.pildoras.IoC;

public class SecretarioEmpleado implements Empleados {
	
	private CreacionInformes informeNuevo;
	private String nombreEmpresa;
	private String email;
	private String email1;

	public void setInformeNuevo(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	@Override
	public String getTareas() {
		// TODO Auto-generated method stub
		return "Gestiono la agenda del jefe y programo la de los empleados";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe de agenda de empleados: " + informeNuevo.getInforme2();
	}

}
